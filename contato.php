<?php
	
	$ok = 0;
	if(isset($_POST["email"])){
		
		$assunto 	= "Site KiBeleza";
		$nome		= $_POST["nome"];
		$email		= $_POST["email"];
		$fone		= $_POST["fone"];
		$mens		= $_POST["mens"];
		
		// Salvar no banco
		require_once("admin/class/class-contato.php");
		
		$data = date('Y-m-d');
		$hora = date('H:i:s');
		
		$contato = new Contato();
		$contato->nome 		= $nome;
		$contato->email		= $email;
		$contato->fone		= $fone;
		$contato->mens		= $mens;
		$contato->data		= $data;
		$contato->Inserir();
		
		//Enviar por e-mail:
		require_once("vendor/PHPMailerAutoload.php");
				
		$phpmail = new PHPMailer(); // Instânciamos a classe PHPmailer para poder utiliza-la          
        $phpmail->isSMTP(); // envia por SMTP
        
        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';
        
        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL
        
        //$phpmail->SMTPSecure = 'tls';
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   
        
        $phpmail->Username = "smpsistema@gmail.com"; // SMTP username         
        $phpmail->Password = "*****"; // SMTP password          
        $phpmail->IsHTML(true);         
        
        $phpmail->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmail->addAddress("smpsistema@gmail.com", $assunto);// E-mail do destinatario/*  
        
        $phpmail->Subject = $assunto; // Assunto do remetende enviado pelo method post
                
        $phpmail->msgHTML("Nome: $nome <br>
                           E-mail: $email <br>
                           Telefone: $fone <br>
                           Mensagem: $mens");
        
        $phpmail->AlrBody = "Nome: $nome \n
                            E-mail: $email \n
                            Telefone: $fone \n
                            Mensagem: $mens";
            
        if($phpmail->send()){              
            $ok = 1;
        }else{
			$ok = 2;
            //echo "Não foi possível enviar a mensagem. Erro: " .$phpmail->ErrorInfo;
        }
         
        // ############## RESPOSTA AUTOMATICA
        $phpmailResposta = new PHPMailer();        
        $phpmailResposta->isSMTP();
        
        $phpmailResposta->SMTPDebug = 0;
        $phpmailResposta->Debugoutput = 'html';
        
        $phpmailResposta->Host = "smtp.gmail.com";         
        $phpmailResposta->Port = 587;
        
        //$phpmailResposta->SMTPSecure = 'tls';
        $phpmailResposta->SMTPAuth = true;   
        
        $phpmailResposta->Username = "smpsistema@gmail.com";         
        $phpmailResposta->Password = "****";          
        $phpmailResposta->IsHTML(true);         
        
        $phpmailResposta->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmailResposta->addAddress($email, "KiBeleza");// E-mail do destinatario/*  
        
        $phpmailResposta->Subject = "Resposta - " .$assunto; // Assunto do remetende enviado pelo method post
                
        $phpmailResposta->msgHTML("Nome: $nome <br>
                            Em breve daremos o retorno");
        
        $phpmailResposta->AlrBody = "Nome: $nome \n
                            Em breve daremos o retorno";
            
        $phpmailResposta->send();
		
	}
	

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta name="Description" content="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
<meta name="Keywords" content="Clínica Kibeleza, Beleza, Salão, Corte">	
<title>Clínica Kibeleza</title>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<link rel="stylesheet" type="text/css" href="css/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/lity.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">	
	
<link rel="stylesheet" type="text/css" href="css/estilo.css">
	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
</head>

<body>
	<!-- Topo -->
	<?php require_once("conteudo/topo.php"); ?>
	<section class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.0254647270517!2d-46.434050784408164!3d-23.495592265053293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce63dda7be6fb9%3A0xa74e7d5a53104311!2sSenac%20S%C3%A3o%20Miguel%20Paulista!5e0!3m2!1spt-BR!2sbr!4v1571837616519!5m2!1spt-BR!2sbr" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	</section>
	
	<section class="site contato">
	
		<article>
			
		</article>
		<article>
			<h2>Formálario contato</h2>
			<form action="#" method="post" class="form">
				<div>
					<input type="text" placeholder="Nome: " required name="nome">
				</div>
				<div>
					<input type="email" placeholder="Email: " required name="email">
				</div>
				<div>
					<input type="tel" placeholder="Telefone: " name="fone">
				</div>
				<div>
					<textarea name="mens" placeholder="Mensagem: " cols="10" rows="10"></textarea>
				</div>
				<div>
					<span class="email-Ok">
						<?php
						
							if($ok == 1){
								echo($nome. ", sua mensagem enviada com sucesso.");
							}else if($ok == 2){
								echo($nome. ", não foi possivél enviar a mensagem.");
							}
						
						?>
					</span>
					<input type="submit" value="ENVIAR">
				</div>
				
				
			</form>
		</article>
		
	</section>
	
	
	<!-- Galeria Insta -->
    <?php require_once("conteudo/galeria.php"); ?>
	<!-- Rodapé -->
	<?php require_once("conteudo/rodape.php"); ?>

	<script type="text/javascript" src="js/jquery-3.4.1.slim.min.js"></script> 
	<script type="text/javascript" src="js/slick.js"></script> 
	<script type="text/javascript" src="js/lity.js"></script> 
	<script type="text/javascript" src="js/wow.js"></script>

	<script type="text/javascript" src="js/anima.js"></script>
</body>
</html>