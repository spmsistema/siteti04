<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">
<meta name="Description" content="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
<meta name="Keywords" content="Clínica Kibeleza, Beleza, Salão, Corte">	
<title>Clínica Kibeleza</title>
<link rel="stylesheet" type="text/css" href="css/slick.css">
<link rel="stylesheet" type="text/css" href="css/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/lity.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">	
	
<link rel="stylesheet" type="text/css" href="css/estilo.css">
	
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
</head>

<body>
	<!-- Topo -->
	<?php require_once("conteudo/topo.php"); ?>
	<!-- Banner -->
	<?php require_once("conteudo/banner.php"); ?>
	
    <section class="servico wow slideInUp">
        <div class="site">
            <h2> Serviços </h2>
            <hr>
			<!-- Lista Servico -->
			<?php require_once("conteudo/lista-servico.php"); ?>
        </div>   
    </section>
	
    <!-- Sobre-->
	<?php require_once("conteudo/sobre.php"); ?>

	
	<!-- Galeria Insta -->
    <?php require_once("conteudo/galeria.php"); ?>
	<!-- Rodapé -->
	<?php require_once("conteudo/rodape.php"); ?>

	<script type="text/javascript" src="js/jquery-3.4.1.slim.min.js"></script> 
	<script type="text/javascript" src="js/slick.js"></script> 
	<script type="text/javascript" src="js/lity.js"></script> 
	<script type="text/javascript" src="js/wow.js"></script>

	<script type="text/javascript" src="js/anima.js"></script>
</body>
</html>