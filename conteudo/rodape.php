<footer class="rodape wow slideInUp">
	<div class="site"> 
		<img src="img/logo-kibeleza-branco.svg" alt="Logo clinica Kibeleza">
		<p> It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
		<ul class="redes-sociais">
			<li> 
				<a class="facebook" href="https://www.facebook.com.br/" target="_blank"> Facebook </a>
			</li>
			<li> 
				<a class="twitter" href="https://www.twitter.com.br" target="_blank"> Twitter </a> 
			</li>
			<li> <a class="instagram" href="https://www.instargram.com.br" target="_blank"> Instargram </a> 
			</li>
		</ul>
	</div>
</footer>