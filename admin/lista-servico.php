<?php

require_once("class/class-conexao.php");

try{
	$conexao = Conexao::LigarConexao();
	$conexao->exec("SET NAMES utf8");
	
	if(!$conexao){
		echo "Não foi possível conectar ao servidor";
	}
	
	$query = $conexao->prepare("SELECT * FROM servico ORDER BY nomeServico ASC");
	
	$query->execute();
	
	$json = "[";
	
	while($resultado = $query->fetch()){
		
		if($json != "["){
			$json .= ",";
		}
		
		$json .= '{"Codigo":		"'.$resultado["idServico"].'",';
		$json .= '"Empresa":		"'.$resultado["idEmpresa"].'",';
		$json .= '"Nome":			"'.$resultado["nomeServico"].'",';
		$json .= '"Valor":			"'.$resultado["valorServico"].'",';
		$json .= '"Status":			"'.$resultado["statusServico"].'",';
		$json .= '"Data":			"'.$resultado["dataCadServico"].'",';
		$json .= '"fotoUm":		"'.$resultado["foto1"].'",';
		$json .= '"fotoDois":		"'.$resultado["foto2"].'",';
		$json .= '"fotoTres":		"'.$resultado["foto3"].'",';
		$json .= '"Descricao":		"'.$resultado["descServico"].'"}';
	}
	
	$json .= "]";
	
	echo $json;
	
	
}catch(Exception $e){
	echo "Erro: ". $e.getMessage();
}

?>